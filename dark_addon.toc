## Interface: 20400
## Title: |cff959ab6Addon|r
## Notes: An addon.
## SavedVariablesPerCharacter: dark_addon_storage
## LoadOnDemand: 0
## DefaultState: enabled
## Version: 1.0.0

libs\util\util.lua
libs\LibStub-1.0\LibStub.lua
libs\CallbackHandler-1.0\CallbackHandler-1.0.xml
libs\LibSharedMedia-3.0\LibSharedMedia-3.0.lua
libs\DiesalTools-1.0\DiesalTools-1.0.lua
libs\DiesalStyle-1.0\DiesalStyle-1.0.lua
libs\DiesalGUI-1.0\DiesalGUI-1.0.xml
libs\DiesalMenu-1.0\DiesalMenu-1.0.xml

dark_addon.xml
