

dark_addon.rotation = {
  classes = {
    druid = 11,
    hunter = 3,
    mage = 8,
    paladin = 2,
    priest = 5,
    rogue = 4,
    shaman = 7,
    warlock = 9,
    warrior = 1
  },
  rotation_store = { },
  spellbooks = { },
  talentbooks = { },
  dispellbooks = { },
  active_rotation = false
}

function dark_addon.rotation.register(config)
  if config.gcd then
    setfenv(config.gcd, dark_addon.environment.env)
  end
  if config.combat then
    setfenv(config.combat, dark_addon.environment.env)
  end
  if config.resting then
    setfenv(config.resting, dark_addon.environment.env)
  end
  dark_addon.rotation.rotation_store[config.name] = config
end

function dark_addon.rotation.load(name)
  local rotation
  for _, rot in pairs(dark_addon.rotation.rotation_store) do
    if rot.name == name then
      rotation = rot
    end
  end

  if rotation then
    dark_addon.settings.store('active_rotation', name)
    dark_addon.rotation.active_rotation = rotation
    dark_addon.interface.buttons.reset()
    if rotation.interface then
      rotation.interface(rotation)
    end
    dark_addon.log('Loaded rotation: ' .. name)
    dark_addon.interface.status('Ready...')
  else
    dark_addon.error('Unload to load rotation: ' .. name)
  end
end

local loading_wait = false
local ticker
local function init()
  if not loading_wait then
    C_Timer.After(0.3, function()
      dark_addon.rotation.spellbook_map = {
        [1] = dark_addon.rotation.spellbooks.warrior,
        [2] = dark_addon.rotation.spellbooks.paladin,
        [3] = dark_addon.rotation.spellbooks.hunter,
        [4] = dark_addon.rotation.spellbooks.rogue,
        [5] = dark_addon.rotation.spellbooks.priest,
        [7] = dark_addon.rotation.spellbooks.shaman,
        [8] = dark_addon.rotation.spellbooks.mage,
        [9] = dark_addon.rotation.spellbooks.warlock,
        [11] = dark_addon.rotation.spellbooks.druid
      }
      local active_rotation = dark_addon.settings.fetch('active_rotation', false)
      if active_rotation then
        dark_addon.rotation.load(active_rotation)
        dark_addon.interface.status('Ready...')
      else
        dark_addon.interface.status('Load a rotation...')
      end
      loading_wait = false
    end)
  end
end

dark_addon.on_ready(function()
  init()
  loading_wait = true
end)

--dark_addon.event.register("ACTIVE_TALENT_GROUP_CHANGED", function(...)
--  init()
--  loading_wait = true
--end)
