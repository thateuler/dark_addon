
local InCombat = false
local AutoAttack = false

local function hasbuff(name)
    for i = 1, 20 do
        a = UnitBuff("player", i)
        if a == name then return true end
    end
end

local function common()
    enh = GetWeaponEnchantInfo(); if not enh then cast("Rockbiter Weapon"); return true end
    if not hasbuff("Lightning Shield") then cast("Lightning Shield") end
    if player.power.mana.percent < 50 then macro("/target player"); macro(".modify mana "..player.power.mana.max); macro("/targetlasttarget") end
    if player.health.percent < 50 then macro("/target player"); macro(".modify hp "..player.health.max); macro("/targetlasttarget") end
end
setfenv(common, dark_addon.environment.env)

local function combat()
    if common() then return end
    if InCombat and not AutoAttack then cast("Attack") end

    if target.exists and target.alive and target.enemy then
        macro(".damage 100000")
        --if target.castable("Earth Shock") then return cast("Earth Shock") end
        --if target.castable("Lightning Bolt") then return cast("Lightning Bolt") end
    end
end

local function resting()
    if common() then return end
    if target.exists and target.alive and target.enemy then
        --print("you should be fighting...")
        macro(".damage 100000")
        --return cast("Lightning Bolt")
    end
end

dark_addon.rotation.register({
 -- class = dark_addon.rotation.classes.shaman,
  name = 'shaman',
  label = 'Bundled Shaman',
  combat = combat,
  resting = resting
})

dark_addon.event.register("PLAYER_REGEN_ENABLED", function() InCombat = false end)
dark_addon.event.register("PLAYER_REGEN_DISABLED", function() InCombat = true end)
dark_addon.event.register("PLAYER_LEAVE_COMBAT", function() AutoAttack = false end)
dark_addon.event.register("PLAYER_ENTER_COMBAT", function() AutoAttack = true end)
